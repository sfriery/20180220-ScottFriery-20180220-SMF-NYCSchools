This app employs a simple ListView to create a list of NY City Schools. 

Specifically, I have done the following:

• Constructed and populated a ListView
• Customized its layout
• Styled and beautified the ListView
• Optimize its performance

==================================
Adapters: Servants of the ListView
==================================

For ListView customization, it uses a simple custom Adapter

What Exactly is an Adapter?

An adapter loads the information to be displayed from a data source, such as an array or database query and creates a view for each item. Then it inserts the views 
into the ListView. 

Adapters not only exist for ListViews, but for other kinds of views as well; ListView is a subclass of AdapterView, so you can populate it by binding it to an 
adapter. 

            ========         =======         =================================  
			ListView <-----> Adapter <-----> Data Source(ArrayList<NYCSchool>)
            ========         =======         =================================  

Hence, the adapter acts as the middle man between the ListView and its data source, or provider. 

It works kind of like this: 

The ListView asks the adapter what it should display, and the adapter jumps into action: 

• It fetches items to be displayed from data source
• It decides how each item should be displayed
• It then passes this information on to the ListView

In short, The ListView isn't very smart, but when given the right inputs it does a fine job. It fully relies on the adapter to tell it what to display and how to 
display it.

=====================================
School Data Output: Decorator Pattern
=====================================

The output, in this case, is an HTML page displayed in a WebView. NYCSchool represents itself as the main HTML widget, and adds the SATInfo as HTML as well. Each is
triggered by a .toHtml() method. 

The decorator works as follows:

            ==================       ================ 
			NYCSchool.toHtml() ----> SATInfo.toHtml()
            ================== |     ================  
			                   |
                               |     ================================							   
							   ----> Other SchoolInfo Object.toHtml() (if any needed)
							         ================================
			